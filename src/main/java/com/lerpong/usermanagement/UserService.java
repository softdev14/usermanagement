/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.lerpong.usermanagement;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author arpao
 */
public class UserService {
    private static User superAdmin = new User("super", "super");
    private static ArrayList<User> userList = null;
    private static User currentUser = null;
    static {
        userList = new ArrayList<>();
        load();
    }
    public static boolean addUser(User user) {
        userList.add(user);
        save();
        return true;
    }
    
    public static boolean delUser(User user) {
        userList.remove(user);
        save();
        return true;
    }
    
    public static boolean delUser(int index) {
        userList.remove(index);
        save();
        return true;
    }
    
    public static ArrayList<User> getUsers() {
        return userList;
    }
    
    public static boolean updateUser(int index, User user) {
        userList.set(index, user);
        save();
        return true;
    }
    
    public static void save() {
        File file = null;
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {  
            file = new File("user.bin");
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(userList);
            fos.close();
            oos.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public static void load() {
        File file = null;
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        try {  
            file = new File("user.bin");
            fis = new FileInputStream(file);
            ois = new ObjectInputStream(fis);
            userList = (ArrayList<User>) ois.readObject();
            fis.close();
            ois.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(UserService.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public static User getUser(int index) {
        return userList.get(index);
    }
    
    public static User login(String userName, String password){
        if(userName.equals("super") && password.endsWith("super")) {
            return superAdmin;
        }
        for(int i=0; i<userList.size(); i++){
            User user = userList.get(i);
            if(user.getUserName().equals(userName) && user.getPassword().equals(password)) {
                currentUser = user;
                return user;
            }
        }
        return null;
    }
    
    public static boolean isLogin() {
        return currentUser != null;
    }
    
    public static User getCurrentUser() {
        return currentUser;
    }
    
    public static void logout() {
        currentUser = null;
    }
}
